﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace prog_milestone3_10009751
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "NO NAME ENTERED";
            string address = "NO ADDRESS ENTERED";
            string phone = "NO PHONE ENTERED";

            Console.Write(new string('\n', 4));
            Appearance.colorCentered("Welcome to dumbminos Pizza!", "Red");
            Console.Write(new string('\n', 2));
            Appearance.colorIndented("Please enter your name: ", "White");
            name = Console.ReadLine();
            Appearance.colorIndented("Please enter your address: ", "White");
            address = Console.ReadLine();
            Appearance.colorIndented("Please enter your phone number: ", "White");
            phone = Console.ReadLine();
            Customer customer1 = new Customer(name, address, phone);
            var pizzachoice = '0';
            while (pizzachoice == '0')
            {
                Console.Clear();
                Console.Write(new string('\n', 8));
                Appearance.colorCentered("Please select a pizza", "Yellow");
                Console.Write(new string('\n', 2));
                Appearance.colorCentered(" Pizza                     SM    LRG  ", "Green");
                Functions.getpizzas();
                Console.Write(new string('\n', 1));
                Appearance.colorCentered($"Total cost so far:              ${Functions.totalprice.ToString("0.00")}", "White");
                Console.Write(new string('\n', 1));
                Appearance.colorCentered("5. Finished Pizza selection           ", "White");
                pizzachoice = Console.ReadKey(false).KeyChar;
                switch (pizzachoice)
                {
                    case '1':
                        Functions.addpizza('1');
                        pizzachoice = '0';
                        break;
                    case '2':
                        Functions.addpizza('2');
                        pizzachoice = '0';
                        break;
                    case '3':
                        Functions.addpizza('3');
                        pizzachoice = '0';
                        break;
                    case '4':
                        Functions.addpizza('4');
                        pizzachoice = '0';
                        break;
                    case '5':
                        pizzachoice = 'z';
                        break;
                    default:
                        Console.Clear();
                        Console.Write(new string('\n', 10));
                        Appearance.colorCentered("Sorry, invalid selection.", "White");
                        Thread.Sleep(1000);
                        Appearance.colorCentered("You will now return to the main menu", "White");
                        Thread.Sleep(2000);
                        pizzachoice = '0';
                        break;
                }
            }
            var drinkchoice = '0';
            while (drinkchoice == '0')
            {
                Console.Clear();
                Console.Write(new string('\n', 8));
                Appearance.colorCentered("Please select a drink", "Yellow");
                Console.Write(new string('\n', 2));
                Functions.getdrinks();
                Console.Write(new string('\n', 1));
                Appearance.colorCentered($"Total cost so far:      ${Functions.totalprice.ToString("0.00")}", "White");
                Console.Write(new string('\n', 1));
                Appearance.colorCentered("4. Finished drink selection", "White");
                drinkchoice = Console.ReadKey(false).KeyChar;
                switch (drinkchoice)
                {
                    case '1':
                        Functions.adddrink('1');
                        drinkchoice = '0';
                        break;
                    case '2':
                        Functions.adddrink('2');
                        drinkchoice = '0';
                        break;
                    case '3':
                        Functions.adddrink('3');
                        drinkchoice = '0';
                        break;
                    case '4':
                        drinkchoice = 'z';
                        break;
                    default:
                        Console.Clear();
                        Console.Write(new string('\n', 10));
                        Appearance.colorCentered("Sorry, invalid selection.", "White");
                        Thread.Sleep(1000);
                        Appearance.colorCentered("You will now return to the main menu", "White");
                        Thread.Sleep(2000);
                        drinkchoice = '0';
                        break;
                }
            }
            Console.Clear();
            Console.Write(new string('\n', 10));
            Appearance.colorCentered($"Thank you for your order {customer1.Name}. ", "White");
            Appearance.colorCentered($"Your order is:  ", "Yellow");
            Console.Write(new string('\n', 1));
            Functions.getorder();
            Console.Write(new string('\n', 1));
            Appearance.colorCentered($"Total price of: ${Functions.totalprice.ToString("0.00")}", "Yellow");
            Console.Write(new string('\n', 1));
            Appearance.colorCentered($"Press any key to continue", "Gray");
            Console.ReadKey();
            // Take Payment from customer, calculate change.
            Console.Clear();
            Console.Write(new string('\n', 1));
            Appearance.colorCentered("Please enter your Credit Card details:", "White");
            Thread.Sleep(2000);
            Console.Clear();
            Console.Write(new string('\n', 1));
            Appearance.colorCentered("Just kidding!", "White");
            Thread.Sleep(2000);
            Functions.payment();
            Console.Write(new string('\n', 1));
        }
    }
    class Functions
    {
        public static double totalprice = 0.00;
        public static void getpizzas()
        {
            foreach (var x in Food.pizzalist())
            {
                Appearance.colorCentered($"{x.Key}. {x.Value.Item1}  {x.Value.Item2}  {x.Value.Item3}", "White");
            }
        }
        public static void addpizza(char pizza)
        {
            foreach (var x in Food.pizzalist())
            {
                if (pizza == x.Key)
                {
                    Console.Clear();
                    Console.Write(new string('\n', 8));
                    Appearance.colorCentered("Please select size", "White");
                    Appearance.colorCentered("1. Small", "White");
                    Appearance.colorCentered("2. Large", "White");
                    char size = Console.ReadKey(false).KeyChar;
                    if (size == '1')
                    {
                        totalprice = totalprice + x.Value.Item2;
                        addorder($"{x.Value.Item1} Small");
                    }
                    else
                    {
                        totalprice = totalprice + x.Value.Item3;
                        addorder($"{x.Value.Item1} Large");
                    }
                }
            }
        }
        public static void getdrinks()
        {
            foreach (var x in Food.drinklist())
            {
                Appearance.colorCentered($"{x.Key}. {x.Value.Item1} {x.Value.Item2}", "White");
            }
        }
        public static void adddrink(char drink)
        {
            foreach (var x in Food.drinklist())
            {
                if (drink == x.Key)
                {
                    totalprice = totalprice + x.Value.Item2;
                    addorder($"{x.Value.Item1}");
                    Console.Clear();
                    Console.Write(new string('\n', 8));
                    Appearance.colorCentered($"Added: {x.Value.Item1}", "White");
                    Thread.Sleep(1500);
                }
            }
        }
        public static List<string> order = new List<string>();
        public static void addorder(string food)
        {
            order.Add(food);
        }
        public static void getorder()
        {
            foreach (string x in order)
            {
                Appearance.colorCentered(x, "White");
            }
        }
        public static void payment()
        {
            Console.Clear();
            Console.Write(new string('\n', 6));
            Appearance.colorIndented($"Please enter a payment value: ", "White");
            double payment = double.Parse(Console.ReadLine());
            // Add tryparse and trycatch here
            // Ran out of time. Poor planning on my part.
            var change = (payment - totalprice).ToString("0.00");
            Appearance.colorIndented($"Your change is: ${change}", "White");
        }
    }
    class Appearance
    {
        public static void colorCentered(string value, string colorChoice)
        {
            Console.SetCursorPosition((Console.WindowWidth - value.Length) / 2, Console.CursorTop);
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), colorChoice);
            Console.WriteLine(value);
            Console.ResetColor();
        }
        public static void colorIndented(string value, string colorChoice)
        {
            Console.SetCursorPosition(10, Console.CursorTop);
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), colorChoice);
            Console.Write(value);
            Console.ResetColor();
        }
    }
    class Customer
    {
        public Customer(string name, string address, string phone)
        {
            Name = name;
            Address = address;
            Phone = phone;
        }

        public string Name;
        public string Address;
        public string Phone;
    }
    class Food
    {
        // Pizza Dictionary. Code is designed around the hope of easily adding and removing Food items without the need for rewrite.
        public static Dictionary<char, Tuple<string, double, double>> pizzalist()
        {
            var pizzas = new Dictionary<char, Tuple<string, double, double>>();

            pizzas.Add('1', Tuple.Create("Ham and Cheese       ", 8.95, 10.95));
            pizzas.Add('2', Tuple.Create("Ham and Pineapple    ", 8.95, 10.95));
            pizzas.Add('3', Tuple.Create("Meatlovers           ", 8.95, 10.95));
            pizzas.Add('4', Tuple.Create("BBQ Chicken and Bacon", 9.95, 12.95));

            return pizzas;
        }
        public static Dictionary<char, Tuple<string, double>> drinklist()
        {
            var drinks = new Dictionary<char, Tuple<string, double>>();

            drinks.Add('1', Tuple.Create("Coke                 ", 3.97));
            drinks.Add('2', Tuple.Create("Fanta                ", 4.83));
            drinks.Add('3', Tuple.Create("Sprite               ", 3.81));

            return drinks;
        }
    }
}
